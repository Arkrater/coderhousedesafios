# INSTALACION #

* Instalar Unity Hub
* Instalar Unity 2020.3.14f1 (64-bit) de preferencia.
* Visual Studio Community 2019 de preferencia.

# EJECUTAR PROYECTO#

* Buscar carpeta en Unity Hub
* Ejecutar el proyecto con la version de Unity indicada.
* Dar Play

# COPYRIGHT #

Copyright © 2021, Claudio Nuñez. Curso de Desarrollo de Videojuegos de Coderhouse.