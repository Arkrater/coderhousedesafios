using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public new GameObject[] camera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            camera[0].SetActive(false);
            camera[1].SetActive(true);


        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            camera[0].SetActive(true);
            camera[1].SetActive(false);
        }
    }
}
