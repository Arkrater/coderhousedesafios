using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public int lifePlayer = 3;
    public string namePlayer = "Frijolito";
    public float speedPlayer = 6.0f;
    public GameObject swordObject;

    public CharacterController controller;

    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;    

    public Transform cam;

    public Transform groundCheck;
    public LayerMask groundMask;
    public float gravity = -9.81f;
    public float groundDistance = 0.3f;
    public float jumpHeight = 3;
    private Vector3 velocity;
    private bool isGrounded;

    public int maxHealth = 100;
    public int currentHealth;

    public HealthBar healthBar;

    private Animator anim;


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Frame inicial");
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);

        anim = GetComponentInChildren<Animator>();

        /*
        transform.position = new Vector3(-42, 1, 0);
        transform.localScale = new Vector3(2, 2, 2);
        Debug.Assert(lifePlayer > 0, "El personaje sigue vivo");
        Debug.Log(swordObject.GetComponent<SwordController>().GetSwordName());
        swordObject.GetComponent<SwordController>().SetSwordName("Mega Espada");
        Debug.Log(swordObject.GetComponent<SwordController>().GetSwordName());
        swordObject.transform.position = transform.position + Vector3.forward;
        transform.position = new Vector3(0, 1, 0);
        */
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Jump();

        if (Input.GetKeyDown(KeyCode.P) )
        {
            TakeDamage(20);
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            RecoverDamage(20);
        }



    }

    void Movement()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2;
        }

        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            controller.Move(moveDirection * speedPlayer * Time.deltaTime);
            Run();
        }
        else
        {
            Idle();
        }

    }

    void Jump()
    {
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }

    void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
        if (currentHealth == 0)
        {
            lifePlayer--;
            Debug.Log(" vida actual del personaje: " + lifePlayer);
        }
    }

    void RecoverDamage(int heal)
    {
        currentHealth += heal;
        if(currentHealth <= 100)
        {
            healthBar.SetHealth(currentHealth);
        }
        else
        {
            currentHealth = maxHealth;
            lifePlayer++;
            Debug.Log(" vida actual del personaje: " + lifePlayer);
        }
        
    }

    void Idle()
    {
        anim.SetFloat("Speed", 0f, 0.1f, Time.deltaTime);
    }

    void Run()
    {
        anim.SetFloat("Speed", 1f, 0.1f, Time.deltaTime);
    }
}
