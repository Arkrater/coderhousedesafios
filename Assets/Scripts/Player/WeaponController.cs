using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{

    public float cdSpeed;

    public float fireRate;

    public float recoilCD;

    private float accuracy;

    public float maxSpreadAngle;

    public float timeTillMaxSpread;

    public GameObject bullet;

    public GameObject shootPoint;

    private Animator animShoot;
    //public AudioSource gunShot;

    //public AudioClip singleShot;

    // Start is called before the first frame update
    void Start()
    {
        animShoot = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        cdSpeed += Time.deltaTime * 60f;

        if (Input.GetButton("Fire1"))
        {
            accuracy += Time.deltaTime * 4f;
            if(cdSpeed >= fireRate)
            {
                Shoot();
                //gunShot.PlayOneShot(singleShot);
                cdSpeed = 0;
                recoilCD = 1;
            }
        }
        else
        {
            animShoot.SetBool("Shoot", false);
            recoilCD -= Time.deltaTime;
            if(recoilCD <= 1)
            {
                accuracy = 0.0f;
            }
               
        }
    }

    void Shoot()
    {

        animShoot.SetBool("Shoot", true);

        RaycastHit hit;

        Quaternion fireRotation = Quaternion.LookRotation(transform.forward);

        float currentSpread = Mathf.Lerp(0.0f, maxSpreadAngle, accuracy / timeTillMaxSpread);

        fireRotation = Quaternion.RotateTowards(fireRotation, Random.rotation, Random.Range(0.0f, currentSpread));

        if (Physics.Raycast(transform.position, fireRotation * Vector3.forward, out hit, Mathf.Infinity))
        {
            GameObject tempBullet = Instantiate(bullet, shootPoint.transform.position, fireRotation);
            tempBullet.GetComponent<AmmoController>().hitPoint = hit.point;
        }
    }
}
