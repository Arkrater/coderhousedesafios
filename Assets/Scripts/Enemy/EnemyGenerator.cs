using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{

    //Script para la invocacion de bolas de canon

    private bool mIsLoaded = false;

    private Transform cannonBallSpawn;
    public GameObject cannonBall;

    private ParticleSystem smoke;

    public float powerCannon = 12.0f;
    public float timeToStart = 10f;
    public float repeatTime = 6f;
    /*
    public GameObject enemyPrefab;
    public GameObject[] listEnemyPrefabs;
    public float timeToStart = 10f;
    public float repeatTime = 6f;
    int enemyIndex;
    */



    // Script para el movimiento del canon

    [SerializeField] float speed;
    [SerializeField] Transform startPoint, endPoint;
    [SerializeField] float changeDirectionDelay;

    private Transform destinationTarget, departTarget;

    private float startTime;

    private float journeyLenght;

    bool isWaiting;


    // Start is called before the first frame update
    void Start()
    {
        //Instantiate(enemyPrefab, transform);
        /*
        enemyIndex = Random.Range(0, listEnemyPrefabs.Length);
        listEnemyPrefabs[enemyIndex].transform.position = transform.position;
        listEnemyPrefabs[enemyIndex].

        InvokeRepeating("Spawn", timeToStart, repeatTime);
        Debug.Log(enemyIndex);
        */

        // Disparo de bola de canon



        cannonBallSpawn = transform.Find("cannonBallSpawn");

        //smoke = transform.Find("smoke").GetComponent<ParticleSystem>();

        InvokeRepeating("ShootCannonBall", timeToStart, repeatTime);

        //ShootCannonBall();

        //Movimiento de Canon

        departTarget = startPoint;
        destinationTarget = endPoint;

        startTime = Time.time;
        journeyLenght = Vector3.Distance(departTarget.position, destinationTarget.position);

    }

    // Update is called once per frame
    void Update()
    {
        movementCanon();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ShootCannonBallSpacebar();
        }

    }

    private void FixedUpdate()
    {
        
    }

    void Spawn()
    {
        //Instantiate(listEnemyPrefabs[enemyIndex], transform.position, transform.rotation);
    }

    void ShootCannonBall()
    {
        GameObject ball = Instantiate(cannonBall, cannonBallSpawn.position, Quaternion.identity);

        Rigidbody rb = ball.AddComponent<Rigidbody>();

        rb.velocity = powerCannon * cannonBallSpawn.forward;

        StartCoroutine(RemoveCannonBallRB(rb, 3.0f));

        smoke.Play();
    }

    void ShootCannonBallSpacebar()
    {
        GameObject newCannonBall = cannonBall;
        newCannonBall.transform.localScale = new Vector3(10f, 10f, 10f);
        GameObject ball = Instantiate(newCannonBall, cannonBallSpawn.position, Quaternion.identity);
        newCannonBall.transform.localScale = new Vector3(1f, 1f, 1f);
        Rigidbody rb = ball.AddComponent<Rigidbody>();

        rb.velocity = powerCannon * cannonBallSpawn.forward;

        StartCoroutine(RemoveCannonBallRB(rb, 3.0f));
        
        smoke.Play();
    }

    IEnumerator RemoveCannonBallRB(Rigidbody rb, float delay)
    {
        yield return new WaitForSeconds(delay);

        Destroy(rb);        
    }

    void movementCanon()
    {
        if (!isWaiting)
        {
            if(Vector3.Distance(transform.position, destinationTarget.position) > 0.01f)
            {
                float distCovered = (Time.time - startTime) * speed;

                float fractionOfJourney = distCovered / journeyLenght;

                transform.position = Vector3.Lerp(departTarget.position, destinationTarget.position, fractionOfJourney);
            }
            else
            {
                isWaiting = true;
                StartCoroutine(changeDelay());
            }
        }
        
    }

    void ChangeDestination()
    {
        if(departTarget == endPoint && destinationTarget == startPoint)
        {
            departTarget = startPoint;
            destinationTarget = endPoint;
        }
        else
        {
            departTarget = endPoint;
            destinationTarget = startPoint;
        }
    }

    IEnumerator changeDelay()
    {
        yield return new WaitForSeconds(changeDirectionDelay);
        ChangeDestination();
        startTime = Time.time;
        journeyLenght = Vector3.Distance(departTarget.position, destinationTarget.position);
        isWaiting = false;
    }


}
