    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public float speedEnemy = 1.0f;
    public GameObject puntoInicial;
    public GameObject puntoFinal;
    public float count;
    // Start is called before the first frame update
    void Start()
    {
        //transform.position = puntoInicial.transform.position;
        //transform.localScale = puntoInicial.transform.localScale;
        //Debug.DrawLine(transform.position, puntoFinal.transform.position, Color.red, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        count += Time.deltaTime;

        if(count > 6.0f)
        {
            Destroy(this.gameObject);
        }


        //Debug.Log(Time.deltaTime);
        //transform.position += new Vector3(speedEnemy, 0, 0) * Time.deltaTime;
        MoveEnemy();
    }

    private void MoveEnemy()
    {
        transform.Translate(speedEnemy * Time.deltaTime * Vector3.forward);
    }
}
